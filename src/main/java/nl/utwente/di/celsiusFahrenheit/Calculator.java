package nl.utwente.di.celsiusFahrenheit;

/**
 * A temperature calculator.
 */
public class Calculator {
    /**
     * Calculates degrees fahrenheit from degrees celsius.
     *
     * @param celsius The degrees celsius.
     * @return The degrees fahrenheit.
     */
    public double calculateFahrenheit(final String celsius) {
        final double bestCelsius = Double.parseDouble(celsius);
        return bestCelsius * (9.0 / 5.0) + 32.0;
    }
}
