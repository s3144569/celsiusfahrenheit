package ns.utwente.di.bookQuote;

import nl.utwente.di.celsiusFahrenheit.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the quoter.
 */
public class TestCalculator {
    /**
     * Tests the first book.
     */
    @Test
    public void testCalc() throws Exception {
        final Calculator calculator = new Calculator();
        final double fahrenheit = calculator.calculateFahrenheit("0");
        Assertions.assertEquals(0.0, fahrenheit, 0.0, "Celsius to fahrenheit.");
    }
}
